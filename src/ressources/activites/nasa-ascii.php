<?php

register_activity(
    'nasa-ascii',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1typebase',
        'type' => 'url',
        'titre' => "L'étrange parachute de la NASA",
        'auteur' => "Laurent COOPER",
        'URL' => 'index.php?activite=nasa-ascii&page=introduction',
        'commentaire' => "Le 22 février 2021, le rover perseverance de la NASA s'est posé sur Mars. Mais l'étrange motif du parachute avait il une signification?",
        'directory' => 'nasa-ascii',
        'icon' => 'fas fa-cogs',
        'prerequis' => NULL
    )
);
