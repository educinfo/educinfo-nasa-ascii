<?php
/*****************
1ère NSI
Le parachute codé 
******************/
global $pages;

// pas de menu
$menu_activite = array(
    "titre" => "Un codage innatendu",
    "contenu" => ['introduction',
        'binaire',
        'nsibest'
    ]
);

// pages des bases du javascript
$nasa_ascii_pages = array(
    'introduction' => array(
        "template" => 'nasa-ascii/nasa.twig.html',
        "menu" => 'nasa-ascii',
        'page_precedente' => NULL,
        'page_suivante' => 'binaire',
        'titre' => "L'étrange parachute",
        "css" => 'ressources/nasa-ascii/assets/css/nasa-ascii.css'
    ),
    'binaire'=> array(
        "template" => 'nasa-ascii/binaire.twig.html',
        "menu" => 'nasa-ascii',
        'page_precedente' => 'introduction',
        'page_suivante' => 'nsibest',
        'titre' => "Le codage du parachute",
        "css" => 'ressources/nasa-ascii/assets/css/nasa-ascii.css'
    ),
    'nsibest'=> array(
        "template" => 'nasa-ascii/nsibest.twig.html',
        "menu" => 'nasa-ascii',
        'page_precedente' => 'binaire',
        'page_suivante' => NULL,
        'titre' => "Un autre message",
        "css" => 'ressources/nasa-ascii/assets/css/nasa-ascii.css'
    ),
);

$pages = array_merge($pages, $nasa_ascii_pages);
